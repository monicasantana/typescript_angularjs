import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Todo } from '../todo';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  todos: Todo[];
  selectedTodo: Todo;
  addingTodo = false;
  error: any;

  constructor(private router: Router, private todoService: TodoService) {}

  getTodos(): void {
    this.todoService
      .getTodos()
      .subscribe(
        todos => (this.todos = todos),
        error => (this.error = error)
      )
  }

  addTodo(): void {
    this.addingTodo = true;
    this.selectedTodo = null;
  }

  close(savedTodo: Todo): void {
    this.addingTodo = false;
    if (savedTodo) {
      this.getTodos();
    }
  }

  borrarItem(todo: Todo, event: any): void {
    event.stopPropagation();
    this.todoService.delete(todo).subscribe(res => {
      this.todos = this.todos.filter(h => h !== todo);
      if (this.selectedTodo === todo) {
        this.selectedTodo = null;
      }
    }, error => (this.error = error));
  }

  ngOnInit(): void {
    this.getTodos();
  }

  onSelect(todo: Todo): void {
    this.selectedTodo = todo;
    this.addingTodo = false;
  }

  goDetail(): void {
    this.router.navigate(['/detail', this.selectedTodo.id]);
  }
}
