import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Todo } from './todo';

@Injectable()
export class TodoService {
  private todosUrl = 'app/todos'; // URL to web api

  constructor(private http: HttpClient) {}

  // Get tasks
  getTodos() {
    return this.http
      .get<Todo[]>(this.todosUrl)
      .pipe(map(data => data), catchError(this.handleError));
  }

  // Get task
  getTodo(id: number): Observable<Todo> {
    return this.getTodos().pipe(
      map(todos => todos.find(todo => todo.id === id))
    );
  }

  // Save task
  save(todo: Todo) {
    if (todo.id) {
      return this.put(todo);
    }
    return this.post(todo);
  }

  // Delete task
  delete(todo: Todo) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const url = `${this.todosUrl}/${todo.id}`;

    return this.http.delete<Todo>(url).pipe(catchError(this.handleError));
  }

  // Add task
  private post(todo: Todo) {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http
      .post<Todo>(this.todosUrl, todo)
      .pipe(catchError(this.handleError));
  }

  // Change task
  private put(todo: Todo) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const url = `${this.todosUrl}/${todo.id}`;

    return this.http.put<Todo>(url, todo).pipe(catchError(this.handleError));
  }

  private handleError(res: HttpErrorResponse | any) {
    console.error(res.error || res.body.error);
    return observableThrowError(res.error || 'Server error');
  }
}
