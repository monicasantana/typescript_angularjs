import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Todo } from '../todo';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-add-todo-item',
  templateUrl: './add-todo-item.component.html',
  styleUrls: ['./add-todo-item.component.scss']
})
export class AddTodoItemComponent implements OnInit {
  @Input() todo: Todo;
  @Output() close = new EventEmitter();
  todos: Todo[];
  error: any;
  navigated = false;

  constructor(
    private todoService: TodoService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        const id = +params['id'];
        this.navigated = true;
        this.todoService.getTodo(id).subscribe(todo => (this.todo = todo));
      } else {
        this.navigated = false;
        this.todo = new Todo();
      }
    });
  }

  saveTodo(): void {
    this.todoService.save(this.todo).subscribe(todo => {
      this.todo = todo;
      this.goBack(todo);
    }, error => (this.error = error));
  }

  goBack(savedTodo: Todo = null): void {
    this.close.emit(savedTodo);
    if (this.navigated) {
      window.history.back();
    }
  }

  resetTodo(): void {
    this.todoService.delete(this.todo).subscribe(todo => {
      this.todo.todoTxt = "";
    }, error => (this.error = error));
  }
}

