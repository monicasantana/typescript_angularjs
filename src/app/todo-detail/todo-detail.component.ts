import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Todo } from '../todo';
import { TodoService } from '../todo.service';

@Component({
  selector: 'my-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.scss']
})
export class TodoDetailComponent implements OnInit {
  @Input() todo: Todo;
  @Output() close = new EventEmitter();
  todos: Todo[];
  selectedTodo: Todo;
  error: any;
  navigated = false;

  constructor(
    private todoService: TodoService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        const id = +params['id'];
        this.navigated = true;
        this.todoService.getTodo(id).subscribe(todo => (this.todo = todo));
      } else {
        this.navigated = false;
        this.todo = new Todo();
      }
    });
  }

  // Save task
  saveTodo(): void {
    this.todoService.save(this.todo).subscribe(todo => {
      this.todo = todo;
      this.goBack(todo);
    }, error => (this.error = error));
  }

  // Go back
  goBack(savedTodo: Todo = null): void {
    this.close.emit(savedTodo);
    if (this.navigated) {
      window.history.back();
    }
  }

  // Delete task
  borrarItem(todo: Todo, event: any): void {
    event.stopPropagation();
    this.todoService.delete(todo).subscribe(res => {
      this.todos = this.todos.filter(h => h !== todo);
      if (this.selectedTodo === todo) {
        this.selectedTodo = null;
      }
    }, error => (this.error = error));
    this.goBack(todo);
  }

  // Reset value task
  resetDetail(): void {
    this.todoService.save(this.todo).subscribe(todo => {
      this.todo.todoTxt = "";
    }, error => (this.error = error));
  }
}
